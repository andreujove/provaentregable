package com.project.provapractica;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.project.provapractica.Model.Film;
import com.project.provapractica.Model.FilmGhibli;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FifthActivity extends AppCompatActivity {
    Activity activity = this;
    ArrayList<FilmGhibli> filmsList1;
    ListView listView;
    MyAdapter2 adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fifth);
        filmsList1 = new ArrayList<FilmGhibli>();
        listView = findViewById(R.id.listView);
        adapter = new MyAdapter2(activity, R.layout.row2, filmsList1);
        listView.setAdapter(adapter);
        getUsersFromRetrofit();
    }

    private void getUsersFromRetrofit() {
        MyService service = RetrofitClientInstance.getRetrofitInstance().create(MyService.class);
        Call<List<FilmGhibli>> call = service.getFilms();

        call.enqueue(new Callback<List<FilmGhibli>>() {
            @Override
            public void onResponse(Call<List<FilmGhibli>> call, Response<List<FilmGhibli>> response) {
                filmsList1.addAll(response.body());
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<FilmGhibli>> call, Throwable t) {

            }
        });
    }
}
