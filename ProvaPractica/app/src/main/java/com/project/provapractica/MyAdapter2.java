package com.project.provapractica;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.project.provapractica.Model.FilmGhibli;

import java.util.ArrayList;

public class MyAdapter2 extends ArrayAdapter<FilmGhibli> {
        int layoutResourceId;
        Context context;
        ArrayList<FilmGhibli> filmsList;
        TextView title_camp;

        public MyAdapter2 (Context context, int layoutResourceId, ArrayList<FilmGhibli> filmsList){
            super (context, layoutResourceId, filmsList);
            this.context=context;
            this.layoutResourceId=layoutResourceId;
            this.filmsList=filmsList;
        }

        @Override
    public View getView(int position, View convertView, ViewGroup parent){
            View row2;

            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row2 = inflater.inflate(layoutResourceId, parent, false);

             title_camp = row2.findViewById(R.id.title_camp);

             FilmGhibli f = filmsList.get(position);
             title_camp.setText(f.getTitle());
             return row2;
        }

}
