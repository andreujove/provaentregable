package com.project.provapractica;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.project.provapractica.Controller.FilmController;
import com.project.provapractica.Model.Film;
import com.squareup.picasso.Picasso;

public class FourthActivity extends AppCompatActivity {

    FilmController controller;
    Film film;
    TextView title;
    TextView points;
    TextView description;
    TextView yearOfPublication;
    ImageView imageUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fourth);
        String id = getIntent().getStringExtra("id");

        controller = FilmController.get(this);
        film = controller.getFilm(id);

        title = findViewById(R.id.tv_title1);
        points = findViewById(R.id.tv_points1);
        description = findViewById(R.id.tv_description1);
        yearOfPublication = findViewById(R.id.tv_yearOfPublication1);
        imageUrl = findViewById(R.id.tv_imageView1);
        showFilm();
    }

    private void showFilm() {
        title.setText(film.getTitle());
        points.setText(String.valueOf(film.getPoints()));
        description.setText(film.getDescription());
        yearOfPublication.setText(String.valueOf(film.getYearOfPublication()));
        Picasso.get().load(film.getImageUrl()).into(imageUrl);
    }

    public void deleteFilm(View view) {
        controller.deleteFilm(film);
        finish();
    }
}
