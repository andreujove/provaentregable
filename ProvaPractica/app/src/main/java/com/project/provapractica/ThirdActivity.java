package com.project.provapractica;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.project.provapractica.Controller.FilmController;
import com.project.provapractica.Model.Film;

public class ThirdActivity extends AppCompatActivity {

    EditText et_title;
    EditText et_description;
    EditText et_year;
    EditText et_points;
    EditText et_imageUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);
        et_title = findViewById(R.id.et_title);
        et_description = findViewById(R.id.et_description);
        et_year = findViewById(R.id.et_year);
        et_points = findViewById(R.id.et_points);
        et_imageUrl = findViewById(R.id.et_imageUrl);
    }

    public void addFilm(View view) {
        String title = et_title.getText().toString();
        String description = et_description.getText().toString();
        String year = et_year.getText().toString();
        String points = et_points.getText().toString();
        String imageUrl = et_imageUrl.getText().toString();

        if (checkFields(title, description, year, points, imageUrl)){
            Film f = new Film(title, description, Integer.parseInt(year), Integer.parseInt(points), imageUrl);
            FilmController.get(this).createFilm(f);
        }
        et_title.setText("");
        et_description.setText("");
        et_year.setText("");
        et_points.setText("");
        et_imageUrl.setText("");

        finish();
    }

    private boolean checkFields(String title, String description, String year, String points, String imageUrl) {
        boolean result=true;

        if ("".equals(title)) {
            result=false;
            et_title.setError(getString(R.string.firstCamp));
        }
        if ("".equals(description)) {
            result=false;
            et_description.setError(getString(R.string.secondCamp));
        }
        if ("".equals(year)) {
            result=false;
            et_year.setError(getString(R.string.thirdCamp));
        }
        if ("".equals(points)) {
            result=false;
            et_points.setError(getString(R.string.fourthCamp));
        }else {
            if((Integer.parseInt(points)) > 5 && (Integer.parseInt(points)) < 0 ){
                result=false;
                et_points.setError(getString(R.string.setErrorPoints));
            }
        }
        if ("".equals(imageUrl)) {
            result=false;
            et_imageUrl.setError(getString(R.string.fifthCamp));

        }
        return result;
    }
}
