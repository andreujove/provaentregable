package com.project.provapractica.Model;

public class FilmGhibli {


    String title;

    public FilmGhibli (String title){
        this.title= title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
