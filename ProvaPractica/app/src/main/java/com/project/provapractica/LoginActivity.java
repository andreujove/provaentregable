package com.project.provapractica;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    EditText email;
    EditText password;

    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        prefs = getSharedPreferences(getString(R.string.prefs_name), Context.MODE_PRIVATE);
        email = findViewById(R.id.etEmail);
        password = findViewById(R.id.etPassword);
    }

    public void openWebSite(View view){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("https://www.abc.es/play/cine/peliculas/"));
        startActivity(intent);
    }

    public void savePreferences(View view){
        if (email.getText().toString().equals("") && password.getText().toString().equals("")){
            Toast.makeText(this, "Els camps no poden estar buits", Toast.LENGTH_LONG).show();
        } else {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("email", email.getText().toString());
            editor.putString("password", password.getText().toString());
            editor.commit();
            finish();

        }
    }
}
