package com.project.provapractica.Model;


import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.widget.Toast;

import java.util.UUID;


@Entity(tableName = "film")
public class Film {

    @PrimaryKey
    @NonNull
    String title;
    String id;
    String description;
    int yearOfPublication;
    int points;
    String imageUrl;

    public Film(String title, String description, int yearOfPublication, int points, String imageUrl) {
        id = UUID.randomUUID().toString();
        this.title = title;
        this.id = id;
        this.description = description;
        this.yearOfPublication = yearOfPublication;
        this.points = points;
        this.imageUrl = imageUrl;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getYearOfPublication() {
        return yearOfPublication;
    }

    public void setYearOfPublication(int yearOfPublication) {
        this.yearOfPublication = yearOfPublication;
    }

    public int getPoints() {
            return points;
        }

    public void setPoints(int points) {
        this.points = points;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
