package com.project.provapractica;

import com.project.provapractica.Model.Film;
import com.project.provapractica.Model.FilmGhibli;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface MyService {

    @GET("films/")
    Call<List<FilmGhibli>> getFilms();
}
