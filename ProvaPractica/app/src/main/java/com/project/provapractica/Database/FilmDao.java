package com.project.provapractica.Database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.project.provapractica.Model.Film;

import java.util.List;

@Dao
public interface FilmDao {
    @Query("SELECT * From Film")
    List<Film> getFilms();

    @Query("SELECT * FROM Film where id like :uuid")
            Film getFilm(String uuid);

    @Insert
    void addFilm(Film f);

    @Delete
    void deleteFilm(Film f);

    @Update
    void updateFilm(Film f);

}
