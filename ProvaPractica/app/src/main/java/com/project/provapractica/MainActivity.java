package com.project.provapractica;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.project.provapractica.Controller.FilmController;
import com.project.provapractica.Model.Film;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Activity activity = this;
    ListView listView;
    MyAdapter adapter;
    SharedPreferences prefs;
    ArrayList<Film> data;
    FilmController controller;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        prefs = getSharedPreferences(getString(R.string.prefs_name), Context.MODE_PRIVATE);
        String email = prefs.getString("email", "");
        controller = FilmController.get(this);

        if (email.equals("")){
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(intent);
        }

        data = new ArrayList<Film>();
        listView = findViewById(R.id.lv_films);
        adapter = new MyAdapter(activity, R.layout.row, data);
        listView.setAdapter(adapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, FourthActivity.class);
                intent.putExtra("id", data.get(position).getId());
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        showFilms();
    }

    private void showFilms() {
        data.clear();
        data.addAll(controller.getFilms());
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId())
        {
            case R.id.button_films:
                Intent intent = new Intent(MainActivity.this, FifthActivity.class);
                startActivity(intent);
                break;


            case R.id.button_add:
                Intent intent1 = new Intent(MainActivity.this, ThirdActivity.class );
                startActivity(intent1);
                break;
        }

        return (super.onOptionsItemSelected(item));
    }


}
