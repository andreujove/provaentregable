package com.project.provapractica;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.project.provapractica.Model.Film;

import java.util.ArrayList;
import java.util.List;

public class MyAdapter extends ArrayAdapter<Film> {
    int layoutResourId;
    Context context;
    ArrayList<Film> data;

    public MyAdapter(Context context, int layoutResourId, ArrayList<Film> data) {
        super(context, layoutResourId, data);
        this.layoutResourId = layoutResourId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        row = inflater.inflate(layoutResourId, parent, false);
        TextView tv_title = (TextView) row.findViewById(R.id.tv_title);
        TextView tv_points = (TextView) row.findViewById(R.id.tv_points);

        Film f = data.get(position);

        tv_title.setText(f.getTitle());
        tv_points.setText(String.valueOf(f.getPoints()));

        if (f.getPoints()< 2){
           tv_title.setTextColor(context.getResources().getColor(R.color.colorAccent));
           tv_points.setTextColor(context.getResources().getColor(R.color.colorAccent));
        } else if (f.getPoints()==2 || f.getPoints() == 3){
            tv_title.setTextColor(context.getResources().getColor(R.color.BlackPanter));
            tv_points.setTextColor(context.getResources().getColor(R.color.BlackPanter));
        }else if (f.getPoints()==4 || f.getPoints() == 5){
            tv_title.setTextColor(context.getResources().getColor(R.color.Green));
            tv_points.setTextColor(context.getResources().getColor(R.color.Green));
        }
        return row;
    }

}
